# eca-sis-etl
Validating Ed-Fi data with json-schema.

This repository contains a very simple example of how a student record can be validated using json-schema.  The example will create a very simple lambda function that will return the student data submitted to an endpoint if it is valid.  If the data is not valid, it will return a message about what validation check failed.

## tl;dr - Running the example
1. Clone the repository
2. Install the packages `npm install`
3. Start the process locally `npm run runlocal`
4. Import the Postman collecton (located in the root directory of the project)
5. Open any of the items in Postman

Assumes you have Node and Postman installed

## Creating Json-Schema files from Ed-Fi's Swagger documentation
The first thing that needs to be done is to convert the Swagger document into json-schema json files.  I found a simple tool called `openapi2jsonschema`. [Info](https://github.com/garethr/openapi2jsonschema) 

It is a python tool, so you will also need Python installed on your machine.  (The generated json files are already included in this repo, so this step is optional).  Once you have installed the tool you simply need to run it while pointed at Ed-Fi's swagger documentation.  

`openapi2jsonschema https://api.ed-fi.org/v3.0.0/api/metadata/data/v3/resources/swagger.json`

The tool will create a complete set of json-schema documents.  The only change I had to make to the generated .json files was to modify the "$ref" links.  The tool didn't handle the path correctly, but it was easy enough to fix.  I did a global search/replace to remove the string '#/definitions/'.

Example:  in edfi_student.json you will see this `"$ref": "#/definitions/edFi_studentVisa"`.  It needs to be changed to `"$ref": "edFi_studentVisa"` 

Once I had the json files updated I copied them to my project's 'models' directory in a subdirectory that matches the version it was created from.

## Serverless: Creating a very simple Lambda that can be deployed to AWS
For this example, I decided to create a simple Lambda function.  To simplify deployment of the function, I used the [Serverless Framework](https://serverless.com).  That way you can deploy it to your own AWS account or run it locally if desired.

I used a very basic serverless.yml file and kept it as simple as possible.  

A local server can be started by running `npm run runlocal`  Once the local server is running, the endpoint can be accessed at `http://localhost:3000`.  The included Postman collection is pointed at a local server.

If you have an AWS account, you can also deploy this example to your account.  To deploy simply run the command `sls deploy` (This assumes you have set your AWS access key and secret).  The lambda function will be deployed to your AWS account and can be accessed via the API Gateway.  

## Node
I also wrote this as a very simple node function.  To get things installed correctly so that you can run it locally, run the command `npm install` to install the required modules.

## Ajv - Another JSON Schema Validator
Now all I needed was a tool to compare data that I receive in the body of a POST request and the json-schema doc that was derived from Ed-Fi's swagger documentation. 

I tried a few different npm modules, but finally settled on [Ajv](https://www.npmjs.com/package/ajv).  It worked and was simple to install.  It also had quite a bit of documentation and examples out there.  I was able to quickly figure out how to include other sub-schemas.  

In a nutshell, you use the tool by adding schemas to an Ajv object.  Schemas are created by reading in json-schema files and converting them to json.  Once all the schemas have been added to the Ajv object, they are compiled to create a validator.  This validator then can be used to inspect any data object.

If the data is valid, the Validator returns true.  If it isn't valid, then an array of errors will be returned.