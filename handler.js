// Import library for accessing json files.
const fs = require('fs');

// Let's create a uuid for out id
const uuidv4 = require('uuid/v4');

// More info about Ajv here: https://www.npmjs.com/package/ajv
const Ajv = require('ajv');

module.exports.hello = (event, context, callback) => {

  // We are going to assume that the body of the post request is a student.  
  let request = JSON.parse(event.body);
  request.id = uuidv4();

  // Set some optional values
  const ajvOptions = {
        useDefaults: true,
        allErrors: true};

  // Get json-schema files from the models subdirectory and convert to json.
  // All files were created from the Ed-Fi swagger docs using the openapi2jsonschema tool
  // info here: https://github.com/garethr/openapi2jsonschema
  // 
  // The version value will be passed in via the url.  (see the serverless file)
  let mainSchema = JSON.parse(fs.readFileSync(`./models/${event.pathParameters.version}/edfi_student.json`));
  let otherNameSchema = JSON.parse(fs.readFileSync(`./models/${event.pathParameters.version}/edfi_studentothername.json`));
  let identificationDocumentSchema = JSON.parse(fs.readFileSync(`./models/${event.pathParameters.version}/edfi_studentidentificationdocument.json`));
  let personalIdentificationDocumentSchema = JSON.parse(fs.readFileSync(`./models/${event.pathParameters.version}/edfi_studentpersonalidentificationdocument.json`));
  let visaSchema = JSON.parse(fs.readFileSync(`./models/${event.pathParameters.version}/edfi_studentvisa.json`));

  // Create an ajv object with the student schema as the main one that will be validated against
  let ajv = new Ajv(mainSchema, ajvOptions);

  // Now load the other schemas that will be used to validate the other objects
  ajv.addSchema(otherNameSchema, 'edFi_studentOtherName');
  ajv.addSchema(visaSchema, 'edFi_studentVisa');
  ajv.addSchema(identificationDocumentSchema, 'edFi_studentIdentificationDocument');
  ajv.addSchema(personalIdentificationDocumentSchema, 'edFi_studentPersonalIdentificationDocument');

  // This creates a function that will be used to check input
  let validate = ajv.compile(mainSchema);

  //  Check to see if it is valid
  let valid = validate(request);

  // Let's assume that the request was correct, prepare to send it back to the requester
  let body = request;

  if (!valid) {
    // Not valid.  Let's send the errors back instead
    body = validate.errors
  }

  const response = {
    statusCode: 200,
    body: JSON.stringify(body)
  };

  callback(null, response);

};
